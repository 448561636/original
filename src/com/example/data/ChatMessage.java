package com.example.data;

import java.util.HashMap;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.avos.avoscloud.AVMessage;
import com.avos.avoscloud.AVUtils;

/**
 * 消息实体类
 * 
 * @author Zeng
 * 
 */
public class ChatMessage {
	public enum MessageType {
		Status(-5), ComingText(-4), SendingText(-3), ComingImage(-2), SendingImage(
				-1), ComingAudio(0), SendingAudio(1);

		private final int type;

		MessageType(int type) {
			this.type = type;
		}

		public int getType() {
			return this.type;
		}
	}

	private MessageType messageType;
	private String messageContent;
	private String messageFrom;
	private AVMessage internalMessage;
	private String voiseTime;

	public String getVoiseTime() {
		return voiseTime;
	}

	public void setVoiseTime(String voiseTime) {
		this.voiseTime = voiseTime;
	}

	public ChatMessage() {
		this.internalMessage = new AVMessage();
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public String getMessageFrom() {
		return messageFrom;
	}

	public void setMessageFrom(String messageFrom) {
		this.messageFrom = messageFrom;
	}

	public void setFromPeerId(String peerId) {
		this.internalMessage.setFromPeerId(peerId);
	}

	public String getFromPeerId() {
		return internalMessage.getFromPeerId();
	}

	public String getGroupId() {
		return this.internalMessage.getGroupId();
	}

	public void setGroupId(String groupId) {
		this.internalMessage.setGroupId(groupId);
	}

	public void setToPeerIds(List<String> peerIds) {
		this.internalMessage.setToPeerIds(peerIds);
	}

	public List<String> getToPeerIds() {
		return this.internalMessage.getToPeerIds();
	}

	/**
	 * handle coming message
	 * 
	 * @param message
	 */
	public void fromAVMessage(AVMessage message) {
		this.internalMessage = message;
		if (!AVUtils.isBlankString(internalMessage.getMessage())) {
			HashMap<String, Object> params = JSON.parseObject(
					internalMessage.getMessage(), HashMap.class);
			this.messageContent = (String) params.get("content");
			this.messageFrom = (String) params.get("dn");
			this.messageType = (MessageType
					.valueOf((String) params.get("type")));
			switch (this.messageType.getType()) {
			case 0:
			case 1:
				this.voiseTime = (String) params.get("voiceTime");
				break;
			default:
				break;
			}
		}
	}

	/**
	 * 生成消息
	 * 
	 * @return
	 */
	public AVMessage makeMessage() {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("type", this.messageType);
		params.put("dn", this.messageFrom);
		params.put("content", this.messageContent);
		switch (this.messageType.getType()) {
		case 0:
		case 1:
			params.put("voiceTime", this.voiseTime);
			break;
		default:
			break;
		}
		internalMessage.setMessage(JSON.toJSONString(params));
		internalMessage.setTransient(false);
		if (!AVUtils.isBlankString(internalMessage.getFromPeerId())) {
			// 设置为回执消息
			internalMessage.setRequestReceipt(true);
		} else if (!AVUtils.isBlankString(internalMessage.getGroupId())) {
			internalMessage.setRequestReceipt(false);
		}
		return internalMessage;
	}
}
