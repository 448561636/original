package com.example.data;

public interface MessageListenerInterface {

	public void onMessage(ChatMessage msg);

}
