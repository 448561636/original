package com.example;

import java.util.HashMap;

import Common.Constant;
import android.app.Application;

import com.avos.avoscloud.AVInstallation;
import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.PushService;
import com.example.activity.ChatActivity;

public class MyApplication extends Application {
	private static HashMap<String, String> userNameCache = new HashMap<String, String>();
	private static String selfId = null;

	@Override
	public void onCreate() {
		super.onCreate();
		AVOSCloud.initialize(this, Constant.APPID, Constant.APPKEY);
		AVInstallation.getCurrentInstallation().saveInBackground();
		PushService.setDefaultPushCallback(this, ChatActivity.class);
		AVOSCloud.setDebugLogEnabled(true);
	}

	public static String lookupname(String peerId) {
		return userNameCache.get(peerId);
	}

	public static void registerLocalNameCache(String peerId, String name) {
		userNameCache.put(peerId, name);
	}

	public static void setSelfId(String id) {
		selfId = id;
	}

	public static String getSelfId() {
		return selfId;
	}
}
