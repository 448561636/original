package com.example.helper;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.data.ChatEmoji;
import com.example.leanclouddemo.R;
import com.example.util.FaceConversionUtil;

public class BottomLinearLayout extends LinearLayout implements
		OnItemClickListener, OnClickListener {
	private Context context;

	/** 表情页的监听事件 */
	private OnCorpusSelectedListener mListener;

	/** 显示表情页的viewpager */
	private ViewPager vp_face;

	/** 表情页界面集合 */
	private ArrayList<View> pageViews;

	/** 游标显示布局 */
	private LinearLayout layout_point;

	/** 游标点集合 */
	private ArrayList<ImageView> pointViews;

	/** 表情集合 */
	private List<List<ChatEmoji>> emojis;

	/** 表情区域 */
	private View view;
	/**
	 * 添加附件区域
	 */
	private View addView;
	/** 语音区域 */
	private View voiceView;
	/** 输入框 */
	private EditText et_sendmessage;
	private ImageButton btnVoice, btnFace, btnAdd;
	private Button btnSend;

	/** 表情数据填充器 */
	private List<FaceAdapter> faceAdapters;

	/** 当前表情页 */
	private int current = 0;

	public BottomLinearLayout(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public BottomLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}

	// public BottomLinearLayout(Context context, AttributeSet attrs, int
	// defStyle) {
	// super(context, attrs, defStyle);
	// this.context = context;
	// }

	public void setOnCorpusSelectedListener(OnCorpusSelectedListener listener) {
		mListener = listener;
	}

	/**
	 * 表情选择监听
	 */
	public interface OnCorpusSelectedListener {

		void onCorpusSelected(ChatEmoji emoji);

		void onCorpusDeleted();
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		emojis = FaceConversionUtil.getInstace().emojiLists;
		onCreate();
	}

	private void onCreate() {
		Init_View();
		Init_viewPager();
		Init_Point();
		Init_Data();
	}

	/**
	 * 填充数据
	 */
	private void Init_Data() {
		vp_face.setAdapter(new ViewPagerAdapter(pageViews));

		vp_face.setCurrentItem(1);
		current = 0;
		vp_face.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				current = arg0 - 1;
				// 描绘分页点
				draw_Point(arg0);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});

	}

	/**
	 * 初始化游标
	 */
	private void Init_Point() {
		pointViews = new ArrayList<ImageView>();
		ImageView imageView;
		for (int i = 0; i < pageViews.size(); i++) {
			imageView = new ImageView(context);
			imageView.setBackgroundResource(R.drawable.d1);
			LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
					new ViewGroup.LayoutParams(LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT));
			layoutParams.leftMargin = 10;
			layoutParams.rightMargin = 10;
			layoutParams.width = 8;
			layoutParams.height = 8;
			layout_point.addView(imageView, layoutParams);
			if (i == 0) {
				imageView.setBackgroundResource(R.drawable.d2);
			} else {
				imageView.setBackgroundResource(R.drawable.d1);
			}
			pointViews.add(imageView);

		}
	}

	/**
	 * 初始化显示表情的viewpager
	 */
	private void Init_viewPager() {
		pageViews = new ArrayList<View>();

		faceAdapters = new ArrayList<FaceAdapter>();
		for (int i = 0; i < emojis.size(); i++) {
			GridView view = new GridView(context);
			FaceAdapter adapter = new FaceAdapter(context, emojis.get(i));
			view.setAdapter(adapter);
			faceAdapters.add(adapter);
			view.setOnItemClickListener(this);
			view.setNumColumns(7);
			view.setBackgroundColor(Color.TRANSPARENT);
			view.setHorizontalSpacing(1);
			view.setVerticalSpacing(1);
			view.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
			view.setCacheColorHint(0);
			view.setPadding(5, 0, 5, 0);
			view.setSelector(new ColorDrawable(Color.TRANSPARENT));
			view.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
					LayoutParams.WRAP_CONTENT));
			view.setGravity(Gravity.CENTER);
			pageViews.add(view);
		}
	}

	/**
	 * 初始化控件
	 */
	private void Init_View() {
		vp_face = (ViewPager) findViewById(R.id.vp_contains);
		et_sendmessage = (EditText) findViewById(R.id.et_message);
		btnSend = (Button) findViewById(R.id.btn_send);
		btnVoice = (ImageButton) findViewById(R.id.btn_voice_text);
		btnFace = (ImageButton) findViewById(R.id.btn_face);
		btnAdd = (ImageButton) findViewById(R.id.btn_addition);
		layout_point = (LinearLayout) findViewById(R.id.iv_image);

		et_sendmessage.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if (s.length() > 0) {
					btnAdd.setVisibility(View.GONE);
					btnSend.setVisibility(View.VISIBLE);
				} else {
					btnSend.setVisibility(View.GONE);
					btnAdd.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
		btnFace.setOnClickListener(this);
		btnAdd.setOnClickListener(this);
		btnVoice.setOnClickListener(this);
		view = findViewById(R.id.ll_facechoose);
		addView = findViewById(R.id.ll_addition);
		voiceView = findViewById(R.id.ll_voice);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_face:
			if (view.getVisibility() == View.VISIBLE) {
				view.setVisibility(View.GONE);
				addView.setVisibility(View.GONE);
				voiceView.setVisibility(View.GONE);
				// TODO:change the image of btnFace
			} else {
				view.setVisibility(View.VISIBLE);
				addView.setVisibility(View.GONE);
				voiceView.setVisibility(View.GONE);
				InputMethodManager imm = (InputMethodManager) context
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				if (imm.isActive()) {
					// 如果开启
					imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,
							InputMethodManager.HIDE_NOT_ALWAYS);
					// 关闭软键盘，开启方法相同，这个方法是切换开启与关闭状态的
				}
				// TODO:change the image of btnFace
			}
			break;
		case R.id.btn_addition:
			if (addView.getVisibility() == View.GONE) {
				addView.setVisibility(View.VISIBLE);
				view.setVisibility(View.GONE);
				voiceView.setVisibility(View.GONE);
			} else if (addView.getVisibility() == View.VISIBLE) {
				addView.setVisibility(View.GONE);
				view.setVisibility(View.GONE);
				voiceView.setVisibility(View.GONE);
			}
			break;
		case R.id.btn_voice_text:
			// TODO:change the image of btnVoice
			if (voiceView.getVisibility() == View.GONE) {
				voiceView.setVisibility(View.VISIBLE);
				et_sendmessage.setText("上滑取消发送");
				et_sendmessage.setEnabled(false);
				addView.setVisibility(View.GONE);
				view.setVisibility(View.GONE);

				btnSend.setVisibility(View.GONE);
				btnAdd.setVisibility(View.VISIBLE);
			} else {
				addView.setVisibility(View.GONE);
				et_sendmessage.setEnabled(true);
				et_sendmessage.getEditableText().clear();
				view.setVisibility(View.GONE);
				voiceView.setVisibility(View.GONE);
			}
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		ChatEmoji emoji = (ChatEmoji) faceAdapters.get(current).getItem(
				position);
		if (emoji.getId() == R.drawable.face_del_icon) {
			int selection = et_sendmessage.getSelectionStart();
			String text = et_sendmessage.getText().toString();
			if (selection > 0) {
				String text2 = text.substring(selection - 1);
				if ("]".equals(text2)) {
					int start = text.lastIndexOf("[");
					int end = selection;
					et_sendmessage.getText().delete(start, end);
					return;
				}
				et_sendmessage.getText().delete(selection - 1, selection);
			}
		}
		if (!TextUtils.isEmpty(emoji.getCharacter())) {
			if (mListener != null)
				mListener.onCorpusSelected(emoji);
			SpannableString spannableString = FaceConversionUtil.getInstace()
					.addFace(getContext(), emoji.getId(), emoji.getCharacter());
			et_sendmessage.append(spannableString);
		}
	}

	/**
	 * 绘制游标背景
	 */
	public void draw_Point(int index) {
		for (int i = 0; i < pointViews.size(); i++) {
			if (index == i) {
				pointViews.get(i).setBackgroundResource(R.drawable.d2);
			} else {
				pointViews.get(i).setBackgroundResource(R.drawable.d1);
			}
		}
	}

}
