package com.example.helper;

import java.io.File;
import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.data.ChatMessage;
import com.example.leanclouddemo.R;
import com.example.util.BitmapUtil;
import com.example.util.FaceConversionUtil;

public class ChatMsgAdapter extends BaseAdapter {
	private Context context;
	private List<ChatMessage> chatList;
	private LayoutInflater inflater;
	private MediaPlayer mp = null;
	private int index = -1;

	public ChatMsgAdapter(Context context, List<ChatMessage> list) {
		this.context = context;
		this.chatList = list;
		this.inflater = LayoutInflater.from(context);
	}

	class ViewHolder {
		CircleImageView head;
		TextView tvMsg, tvTime;
		ImageView image;
		View gap, voice;
		ImageButton start;
		ProgressBar pb;
	}

	@Override
	public int getItemViewType(int position) {
		return chatList.get(position).getMessageType().getType();
	}

	@Override
	public int getViewTypeCount() {
		// 2种布局,coming,sending,每种包括文字，图片，语音
		return 2;
	}

	@Override
	public int getCount() {
		return chatList.size();
	}

	@Override
	public ChatMessage getItem(int arg0) {
		// TODO Auto-generated method stub
		return chatList.get(arg0);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		final ChatMessage m = getItem(position);
		switch (getItemViewType(position)) {
		case -4:// comingText
		case -2:// comingImage
		case 0:// comingAudio
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.item_chat_coming_msg,
						null);
				holder = new ViewHolder();
				holder.head = (CircleImageView) convertView
						.findViewById(R.id.iv_head_coming);
				holder.tvMsg = (TextView) convertView
						.findViewById(R.id.tv_coming_msg);
				holder.image = (ImageView) convertView
						.findViewById(R.id.iv_coming_image);
				holder.gap = (View) convertView
						.findViewById(R.id.ll_coming_gap);
				holder.pb = (ProgressBar) convertView
						.findViewById(R.id.pb_coming_voice);
				holder.start = (ImageButton) convertView
						.findViewById(R.id.ib_coming_start);
				holder.tvTime = (TextView) convertView
						.findViewById(R.id.tv_coming_time);
				holder.voice = (View) convertView
						.findViewById(R.id.ll_coming_voice);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			break;
		default:
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.item_chat_sending_msg,
						null);
				holder = new ViewHolder();
				holder.head = (CircleImageView) convertView
						.findViewById(R.id.iv_head_sending);
				holder.tvMsg = (TextView) convertView
						.findViewById(R.id.tv_sending_msg);
				holder.image = (ImageView) convertView
						.findViewById(R.id.iv_sending_image);
				holder.gap = (View) convertView
						.findViewById(R.id.ll_sending_gap);
				holder.pb = (ProgressBar) convertView
						.findViewById(R.id.pb_sending_voice);
				holder.start = (ImageButton) convertView
						.findViewById(R.id.ib_sending_start);
				holder.tvTime = (TextView) convertView
						.findViewById(R.id.tv_sending_time);
				holder.voice = (View) convertView
						.findViewById(R.id.ll_sending_voice);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			break;
		}
		
		switch (getItemViewType(position)) {
		case -4:// comingText
		case -3:// sendingText
			holder.head.setImageResource(R.drawable.ic_launcher);
			SpannableString spannableString1 = FaceConversionUtil.getInstace()
					.getExpressionString(context, m.getMessageContent());
			holder.tvMsg.setText(spannableString1);
			holder.image.setVisibility(View.GONE);
			holder.tvMsg.setVisibility(View.VISIBLE);
			holder.gap.setVisibility(View.GONE);
			holder.voice.setVisibility(View.GONE);
			break;
		case -2:// comingImage
		case -1:// sendingImage
			holder.head.setImageResource(R.drawable.ic_launcher);
			String path1 = m.getMessageContent();
			Bitmap b1;
			try {
				b1 = BitmapUtil.Bytes2Bimap(BitmapUtil.readStream(path1));
				if (b1 != null)
					holder.image.setImageBitmap(b1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			holder.image.setVisibility(View.VISIBLE);
			holder.tvMsg.setVisibility(View.GONE);
			holder.gap.setVisibility(View.VISIBLE);
			holder.voice.setVisibility(View.GONE);
			break;
		case 0:// comingAudio
		case 1:// sendingAudio
			holder.head.setImageResource(R.drawable.ic_launcher);
			holder.tvTime.setText(m.getVoiseTime());
			holder.start.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (index == -1) {
						index = position;
					}
					// 是否是点击同一个语音文件
					if (position == index) {
						if (mp != null) {
							boolean isPlaying = mp.isPlaying();
							if (isPlaying) {
								mp.stop();
							} else {
								mp.start();
							}
						} else {
							mp = MediaPlayer.create(context, Uri
									.fromFile(new File(m.getMessageContent())));
							try {
								if (mp != null)
									mp.stop();
								mp.prepareAsync();
								mp.start();
							} catch (IllegalStateException e) {
								e.printStackTrace();
								mp.release();
								mp = null;
							}
						}
					} else {
						if (mp != null) {
							mp.stop();
							mp.reset();
							try {
								mp.setDataSource(m.getMessageContent());
								mp.prepareAsync();
								mp.start();
							} catch (IllegalArgumentException e) {
								e.printStackTrace();
								mp.release();
								mp = null;
							} catch (IllegalStateException e) {
								e.printStackTrace();
								mp.release();
								mp = null;
							} catch (IOException e) {
								e.printStackTrace();
								mp.release();
								mp = null;
							}

						} else {
							mp = MediaPlayer.create(context, Uri
									.fromFile(new File(m.getMessageContent())));
							try {
								if (mp != null)
									mp.stop();
								mp.prepareAsync();
								mp.start();
							} catch (IllegalStateException e) {
								e.printStackTrace();
								mp.release();
								mp = null;
							}
						}
					}
				}
			});
			holder.image.setVisibility(View.GONE);
			holder.tvMsg.setVisibility(View.GONE);
			holder.gap.setVisibility(View.GONE);
			holder.voice.setVisibility(View.VISIBLE);
			break;
		}
		return convertView;
	}
	
	public void releaseMediaPlayer(){
		if(mp!=null){
			mp.stop();
			mp.release();
			mp = null;
		}
	}
}
