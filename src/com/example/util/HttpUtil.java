package com.example.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;

public class HttpUtil {

	public static String getBitmapPath(String url) {
		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(url);
		// Header[] reqHeaders = get.getAllHeaders();//请求头,需要设置
		try {
			HttpResponse response = client.execute(get);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream is = null;
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				try {
					is = entity.getContent();
					byte[] buf = new byte[1024];
					int readBytes = -1;
					while ((readBytes = is.read(buf)) != -1) {
						baos.write(buf, 0, readBytes);
					}
				} finally {
					if (baos != null) {
						baos.close();
					}
					if (is != null) {
						is.close();
					}
				}
				Bitmap bitmap = BitmapUtil.Bytes2Bimap(baos.toByteArray());
				if (bitmap != null) {
					FileUtil fu = new FileUtil();
					File file = fu.imageToSDFromInput("picture",
							System.currentTimeMillis() + ".png", bitmap);
					return file.getAbsolutePath();
				} else
					return null;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getVoicePath(String url) {
		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(url);
		// Header[] reqHeaders = get.getAllHeaders();//请求头,需要设置
		try {
			HttpResponse response = client.execute(get);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				InputStream is = null;
				is = entity.getContent();
				FileUtil fu = new FileUtil();
				File file = fu.writeToSDFromInput("voice",
						System.currentTimeMillis() + ".amr", is);
				return file.getAbsolutePath();
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
