package com.example.receiver;

import java.util.HashMap;
import java.util.List;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.avos.avoscloud.AVMessage;
import com.avos.avoscloud.AVMessageReceiver;
import com.avos.avoscloud.LogUtil;
import com.avos.avoscloud.Session;
import com.avos.avospush.notification.NotificationCompat;
import com.example.activity.ChatActivity;
import com.example.data.ChatMessage;
import com.example.data.MessageListenerInterface;
import com.example.leanclouddemo.R;

public class ChatMessageReceiver extends AVMessageReceiver {

	static HashMap<String, MessageListenerInterface> sessionMessageDispatchers = new HashMap<String, MessageListenerInterface>();

	/**
	 * 当与服务器发生交互的过程中的任何错误，都在这里被返回
	 */
	@Override
	public void onError(Context context, Session session, Throwable arg2) {
		Log.d("ChatMessageReceiver", "onError");
		arg2.printStackTrace();
	}

	/**
	 * 处理接收到的消息，一条新消息到达
	 */
	@Override
	public void onMessage(Context context, Session session, AVMessage msg) {
		Log.d("ChatMessageReceiver", "onMessage");
		JSONObject j = JSONObject.parseObject(msg.getMessage());
		ChatMessage message = new ChatMessage();
		MessageListenerInterface listener = sessionMessageDispatchers.get(msg
				.getFromPeerId());
		/*
		 * 这里是demo中自定义的数据格式，在你自己的实现中，可以完全自由的通过json来定义属于你自己的消息格式
		 * 
		 * 用户发送的消息 {"msg":"这是一个消息","dn":"这是消息来源者的名字"}
		 * 
		 * 用户的状态消息 {"st":"用户触发的状态信息","dn":"这是消息来源者的名字"}
		 */

		if (j.containsKey("content")) {

			message.fromAVMessage(msg);
			// 如果Activity在屏幕上不是active的时候就选择发送 通知

			if (listener == null) {
				LogUtil.avlog
						.d("Activity inactive, about to send notification.");
				NotificationManager nm = (NotificationManager) context
						.getSystemService(Context.NOTIFICATION_SERVICE);

				String ctnt = message.getMessageFrom() + "："
						+ message.getMessageContent();
				Intent resultIntent = new Intent(context, ChatActivity.class);
				resultIntent.putExtra(
						ChatActivity.DATA_EXTRA_SINGLE_DIALOG_TARGET,
						msg.getFromPeerId());
				resultIntent.putExtra(Session.AV_SESSION_INTENT_DATA_KEY,
						JSON.toJSONString(message));
				resultIntent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

				PendingIntent pi = PendingIntent.getActivity(context, -1,
						resultIntent, PendingIntent.FLAG_ONE_SHOT);

				Notification notification = new NotificationCompat.Builder(
						context)
						.setContentTitle(
								context.getString(R.string.notif_title))
						.setContentText(ctnt)
						.setContentIntent(pi)
						.setSmallIcon(R.drawable.ic_launcher)
						.setLargeIcon(
								BitmapFactory.decodeResource(
										context.getResources(),
										R.drawable.ic_launcher))
						.setAutoCancel(true).build();
				nm.notify(233, notification);
				LogUtil.avlog.d("notification sent");
			} else {
				// listener.onMessage(JSON.toJSONString(message));
				listener.onMessage(message);
			}
		}
	}

	/**
	 * 消息真正到达用户了
	 */
	@Override
	public void onMessageDelivered(Context context, Session session,
			AVMessage msg) {
		Log.d("ChatMessageReceiver", "onMessageDelivered");
	}

	/**
	 * 消息发送失败了，可能需要在app端进行重试等
	 */
	@Override
	public void onMessageFailure(Context context, Session session, AVMessage msg) {
		Log.d("ChatMessageReceiver", "onMessageFailure");
	}

	/**
	 * 消息发送成功了，发送成功时间是msg.getTimestamp,这个时间是来自服务器端的时间，这样即便是多台设备中间也不会出现时间的混乱
	 */
	@Override
	public void onMessageSent(Context context, Session session, AVMessage msg) {
		// TODO Auto-generated method stub
		Log.d("ChatMessageReceiver", "onMessageSent");
		Log.d("ChatMessageReceiver", msg.getMessage());
	}

	@Override
	public void onPeersUnwatched(Context context, Session session,
			List<String> arg2) {
	}

	@Override
	public void onPeersWatched(Context context, Session session,
			List<String> arg2) {
		// TODO Auto-generated method stub
	}

	/**
	 * 当服务器成功与客户端打开session时产生本次回调
	 */
	@Override
	public void onSessionOpen(Context context, Session session) {
		// TODO Auto-generated method stub
		Log.d("ChatMessageReceiver", "onSessionOpen");
		sendOpenIntent(context);
	}

	/**
	 * 掉线
	 */
	@Override
	public void onSessionPaused(Context context, Session session) {
		// TODO Auto-generated method stub
		Log.d("ChatMessageReceiver", "onSessionPaused");
	}

	/**
	 * 重新连接上
	 */
	@Override
	public void onSessionResumed(Context context, Session session) {
		// TODO Auto-generated method stub
		Log.d("ChatMessageReceiver", "onSessionResumed");
	}

	/**
	 * 当关注的一些peers下线时，产生的调用
	 */
	@Override
	public void onStatusOffline(Context context, Session session,
			List<String> arg2) {
		// TODO Auto-generated method stub
	}

	/**
	 * 当关注的一些peers上线时，产生的调用
	 */
	@Override
	public void onStatusOnline(Context context, Session session,
			List<String> arg2) {
		// TODO Auto-generated method stub
	}

	private void sendOpenIntent(Context context) {
		Intent intent = new Intent(context, ChatActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		context.startActivity(intent);
	}

	public static void registerSessionListener(String peerId,
			MessageListenerInterface listener) {
		sessionMessageDispatchers.put(peerId, listener);
	}

	public static void unregisterSessionListener(String peerId) {
		sessionMessageDispatchers.remove(peerId);
	}

}
