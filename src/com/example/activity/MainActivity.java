package com.example.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVUser;
import com.avos.avoscloud.LogInCallback;
import com.avos.avoscloud.Session;
import com.avos.avoscloud.SessionManager;
import com.avos.avoscloud.SignUpCallback;
import com.example.MyApplication;
import com.example.leanclouddemo.R;
import com.example.util.FaceConversionUtil;

public class MainActivity extends Activity implements View.OnClickListener {

	private EditText nameInput, pwdInput;
	private Button joinButton, signIn;
	private String selfId = null;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);

		joinButton = (Button) findViewById(R.id.button);
		signIn = (Button) findViewById(R.id.signin);
		joinButton.setOnClickListener(this);
		signIn.setOnClickListener(this);
		nameInput = (EditText) findViewById(R.id.userName);
		pwdInput = (EditText) findViewById(R.id.pwd);
		// 加载表情文件，应该在登录成功后加载，此处提前加载
		new Thread(new Runnable() {
			@Override
			public void run() {
				FaceConversionUtil.getInstace().getFileText(getApplication());
			}
		}).start();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	public void onClick(View v) {
		final String name = nameInput.getText().toString();
		String pwd = pwdInput.getText().toString();
		if (name == null || name.equalsIgnoreCase("")) {
			return;
		}
		if (pwd == null || pwd.equalsIgnoreCase("")) {
			return;
		}

		switch (v.getId()) {
		case R.id.button:
			AVUser.logInInBackground(name, pwd, new LogInCallback<AVUser>() {
				@Override
				public void done(AVUser user, AVException e) {
					// 此处的selfId就是之前提到的用户的唯一标识符 Peer ID,
					// 应该替换成你现有用户系统中的唯一标识符，这里以我们提供的的用户系统为例
					selfId = user.getObjectId();
					MyApplication.setSelfId(selfId);
					Log.d("MainActivity", "className=" + user.getClassName());
					Log.d("MainActivity", "userName=" + user.getUsername());
					MyApplication.registerLocalNameCache(selfId,
							user.getUsername());
					Toast.makeText(MainActivity.this,
							"Name:" + name + ",id:" + selfId, Toast.LENGTH_LONG)
							.show();
					Log.d("MainActivity", "Name:" + name + ",id:" + selfId);
					Session session = SessionManager.getInstance(selfId);
					List<String> yourFriends = new ArrayList<String>();
					// add your friends' peerIds
					session.open(yourFriends);
				}
			});
			break;
		case R.id.signin:
			AVUser user = new AVUser();
			user.setUsername(name);
			user.setPassword(pwd);
			// user.setEmail("steve@company.com");

			// 其他属性可以像其他AVObject对象一样使用put方法添加
			// user.put("phone", "213-253-0000");

			user.signUpInBackground(new SignUpCallback() {
				public void done(AVException e) {
					if (e == null) {
						Toast.makeText(MainActivity.this, "success",
								Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(MainActivity.this, "fail",
								Toast.LENGTH_LONG).show();
					}
				}
			});
			break;
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if (selfId != null) {
			Session session = SessionManager.getInstance(selfId);
			if (session.isOpen())
				session.close();
		}
		super.onDestroy();
	}
}
