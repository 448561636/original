package com.example.activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import Common.Constant;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVUtils;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.Session;
import com.avos.avoscloud.SessionManager;
import com.example.MyApplication;
import com.example.data.ChatMessage;
import com.example.data.ChatMessage.MessageType;
import com.example.data.MessageListenerInterface;
import com.example.helper.ChatMsgAdapter;
import com.example.leanclouddemo.R;
import com.example.receiver.ChatMessageReceiver;
import com.example.util.FileUtil;
import com.example.util.HttpUtil;

public class ChatActivity extends Activity implements MessageListenerInterface {
	private ListView lvChat;
	private Button btnSend;
	private EditText etContent;
	private ImageView ivTakePhoto, ivPhoto, ivVoice;
	private Chronometer chronometer;

	private MediaRecorder recorder = null;
	private File voiceFile = null;
	private String voiceTime = null;

	private String selfId = null, targetId = null;
	private List<ChatMessage> msgList = new LinkedList<ChatMessage>();
	private Session session;
	private ChatMsgAdapter chatAdapter;
	/** 录音时按下或抬起时的xy坐标 */
	private float downX, downY, upX, upY;
	private boolean isRecording = false;

	public static final String DATA_EXTRA_SINGLE_DIALOG_TARGET = "single_target_peerId";

	private Handler myHandler = new Handler(Looper.getMainLooper()) {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0:
				chatAdapter.notifyDataSetChanged();
				lvChat.setSelection(msgList.size() - 1);
				break;
			}
		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_chat);

		initView();
		initData();
	}

	private void initData() {
		selfId = MyApplication.getSelfId();
		// TODO:从intent中获取targetId
		if (selfId.equalsIgnoreCase(Constant.PID_ONE)) {
			targetId = Constant.PID_TWO;
		} else if (selfId.equalsIgnoreCase(Constant.PID_TWO)) {
			targetId = Constant.PID_ONE;
		}
		Log.d("ChatActivity", "selfId:" + selfId + ", targetId:" + targetId);
		// 双方必须互相关注，后期不需在此处关注，从好友列表中进入此activity
		session = SessionManager.getInstance(selfId);
		List<String> peerList = new ArrayList<String>(1);
		peerList.add(targetId);
		session.watchPeers(peerList);
		// TODO:load history
		chronometer.setBase(SystemClock.elapsedRealtime());
	}

	private void initView() {
		lvChat = (ListView) findViewById(R.id.lv_chat);
		btnSend = (Button) findViewById(R.id.btn_send);
		etContent = (EditText) findViewById(R.id.et_message);
		ivTakePhoto = (ImageView) findViewById(R.id.iv_take_photo);
		ivPhoto = (ImageView) findViewById(R.id.iv_photo);
		ivVoice = (ImageView) findViewById(R.id.iv_voice);
		chronometer = (Chronometer) findViewById(R.id.voice_chronometer);

		ivVoice.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					if (event.getPointerCount() == 1) {
						downX = event.getX();
						downY = event.getY();
					} else {
						downX = event.getX(0);
						downY = event.getY(0);
					}
					startRecord();
					// startChr();
					break;
				case MotionEvent.ACTION_UP:
					if (event.getPointerCount() == 1) {
						upX = event.getX();
						upY = event.getY();
					} else {
						upX = event.getX(0);
						upY = event.getY(0);
					}
					if ((downY - upY) > 100) {
						// stopChr();
						stopRecord();
						FileUtil.deleteFile(voiceFile);
						voiceFile = null;
						showToast("语音已取消");
					} else {
						// stopChr();
						stopRecord();
						sendMessage(MessageType.SendingAudio,
								voiceFile.getAbsolutePath(),
								voiceFile.getName());
					}
					break;
				}
				return true;
			}
		});

		chatAdapter = new ChatMsgAdapter(this, msgList);
		lvChat.setAdapter(chatAdapter);

		btnSend.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				sendMessage();
			}
		});
		ivTakePhoto.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(i, Constant.TAKC_PHOTO_REQUEST);
			}
		});
		ivPhoto.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				// 开启Pictures画面Type设定为image
				intent.setType("image/*");
				// 使用Intent.ACTION_GET_CONTENT这个Action
				intent.setAction(Intent.ACTION_GET_CONTENT);
				intent.addCategory(Intent.CATEGORY_OPENABLE);
				// 取得相片后返回本画面
				startActivityForResult(intent, Constant.PICK_PHOTO_REQUEST);
			}
		});
	}

	@Override
	public void onMessage(final ChatMessage message) {
		// TODO:如果要做是否已阅，可以在对方进入聊天界面后发送一个statut类型的消息
		switch (message.getMessageType().getType()) {
		case -3:
			message.setMessageType(MessageType.ComingText);
			msgList.add(message);
			chatAdapter.notifyDataSetChanged();
			lvChat.setSelection(msgList.size() - 1);
			break;
		case -1:
			message.setMessageType(MessageType.ComingImage);
			final String urlBitmap = message.getMessageContent();
			new Thread() {
				@Override
				public void run() {
					String p = HttpUtil.getBitmapPath(urlBitmap);
					if (p != null) {
						message.setMessageContent(p);
						msgList.add(message);
						Message m = myHandler.obtainMessage();
						m.what = 0;
						m.sendToTarget();
					}
				}
			}.start();
			break;
		case 1:
			Log.d("ChatActivity", "voiceTime: " + message.getVoiseTime());
			message.setMessageType(MessageType.ComingAudio);
			final String urlVoice = message.getMessageContent();
			new Thread() {
				@Override
				public void run() {
					String p = HttpUtil.getVoicePath(urlVoice);
					if (p != null) {
						message.setMessageContent(p);
						msgList.add(message);
						Message m = myHandler.obtainMessage();
						m.what = 0;
						m.sendToTarget();
					}
				}
			}.start();
			break;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		ChatMessageReceiver.registerSessionListener(targetId, this);
		if (getIntent().getExtras() != null
				&& !AVUtils.isBlankString(getIntent().getExtras().getString(
						Session.AV_SESSION_INTENT_DATA_KEY))) {
			String msg = getIntent().getExtras().getString(
					Session.AV_SESSION_INTENT_DATA_KEY);
			ChatMessage message = JSON.parseObject(msg, ChatMessage.class);
			msgList.add(message);
			chatAdapter.notifyDataSetChanged();
			lvChat.setSelection(msgList.size() - 1);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		ChatMessageReceiver.unregisterSessionListener(targetId);
		chatAdapter.releaseMediaPlayer();
	}

	private void startChr() {
		chronometer.setBase(SystemClock.elapsedRealtime());
		chronometer.start();
	}

	private void stopChr() {
		chronometer.stop();
		voiceTime = chronometer.getText().toString();
		chronometer.setBase(SystemClock.elapsedRealtime());
	}

	private void startRecord() {
		isRecording = true;
		if (recorder == null) {
			recorder = new MediaRecorder();
			FileUtil fu = new FileUtil();
			fu.creatSDDir("/voice");
			try {
				voiceFile = fu.creatFile("/voice/" + System.currentTimeMillis()
						+ ".amr");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			recorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
			recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
			recorder.setOutputFile(voiceFile.getAbsolutePath());
			try {
				recorder.prepare();
				recorder.start();
				startChr();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				stopRecord();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				stopRecord();
			}
		}
	}

	private void stopRecord() {
		if (isRecording) {
			isRecording = false;
			if (recorder != null) {
				recorder.stop();
				recorder.release();
				recorder = null;
				stopChr();
			}
		}
	}

	private void sendMessage() {
		String content = etContent.getText().toString();
		if (content == null || content.equalsIgnoreCase(""))
			return;
		if (targetId == null)
			return;
		etContent.getEditableText().clear();
		ChatMessage message = new ChatMessage();
		message.setFromPeerId(selfId);
		message.setMessageContent(content);
		message.setMessageType(MessageType.SendingText);
		message.setMessageFrom(MyApplication.lookupname(selfId));
		message.setToPeerIds(Arrays.asList(targetId));
		session.sendMessage(message.makeMessage());

		msgList.add(message);
		chatAdapter.notifyDataSetChanged();
		lvChat.setSelection(msgList.size() - 1);
	}

	public void sendMessage(final MessageType type, final String path,
			String fileName) {
		if (path == null)
			return;
		if (targetId == null)
			return;
		final AVFile file;
		try {
			file = AVFile.withAbsoluteLocalPath(fileName, path);
			file.saveInBackground(new SaveCallback() {
				@Override
				public void done(AVException e) {
					if (e == null) {
						String url = file.getUrl();
						ChatMessage message = new ChatMessage();
						message.setFromPeerId(selfId);
						message.setMessageContent(url);
						message.setMessageType(type);
						message.setMessageFrom(MyApplication.lookupname(selfId));
						message.setToPeerIds(Arrays.asList(targetId));
						if (type == MessageType.SendingAudio)
							message.setVoiseTime(voiceTime);
						session.sendMessage(message.makeMessage());

						message.setMessageContent(path);
						message.makeMessage();
						msgList.add(message);
						chatAdapter.notifyDataSetChanged();
						lvChat.setSelection(msgList.size() - 1);
					} else {
						e.printStackTrace();
					}
				}
			});
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if (session.isOpen())
			session.close();
		super.onDestroy();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data != null) {
			Uri uri = data.getData();
			Bitmap photo = null;
			if (uri != null) {
				photo = BitmapFactory.decodeFile(uri.getPath());
			}
			switch (requestCode) {
			case Constant.TAKC_PHOTO_REQUEST:
				if (photo == null) {
					Bundle bundle = data.getExtras();
					if (bundle != null) {
						photo = (Bitmap) bundle.get("data");
					}
				}
				if (photo != null) {
					FileUtil fu = new FileUtil();
					String fileName = System.currentTimeMillis() + ".png";
					File f = fu.imageToSDFromInput("picture", fileName, photo);
					sendMessage(MessageType.SendingImage, f.getAbsolutePath(),
							fileName);
					photo.recycle();
				}
				break;
			case Constant.PICK_PHOTO_REQUEST:
				Cursor cursor = getContentResolver().query(uri, null, null,
						null, null);
				cursor.moveToFirst();
				// String imgNo = cursor.getString(0); //图片编号
				String path = cursor.getString(1); // 图片文件路径
				// String imgSize = cursor.getString(2); //图片大小
				String imgName = cursor.getString(3); // 图片文件名
				sendMessage(MessageType.SendingImage, path, imgName);
				break;
			}
		}
	}
}
